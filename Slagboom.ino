#include <Servo.h>

const int motorPin = 11; 
const int slagboomButtonExternalPin = 2;
const int slagboomButtonInternalPin = 3;
const int trigPin = 9; 
const int echoPin = 10; 
const int signalingPin = 13;
const int slagboomTriggerButtonPin = 7;
const int escPin = 4;

bool slagboomOpened = false;
bool slagboomStateChanging = false;
bool slagboomOpenedExternaly = false;  
bool stopSlagboomMotor;

long slagboomOpenedTime;

Servo ESC;

void setup() { 
  pinMode(motorPin, OUTPUT); 
  pinMode(slagboomButtonExternalPin, INPUT); 
  pinMode(slagboomButtonInternalPin, INPUT); 
  pinMode(slagboomTriggerButtonPin, INPUT);
  pinMode(trigPin, OUTPUT); 
  pinMode(echoPin, INPUT);
  pinMode(signalingPin, OUTPUT);
  pinMode(escPin, OUTPUT);
  ESC.attach(escPin);
  ESC.write(1500);
  Serial.begin(9600);
} 

void loop() { 
  buttonEvents();
  autoCloseSlagboom();
  if(slagboomStateChanging) {
    if(slagboomOpened && !stopSlagboomMotor) {
      checkSensor();
      if(!stopSlagboomMotor) closeSlagboom();
    }
    else openSlagboom();
    knipperlicht();
  }
} 
 
void buttonEvents() { 
  if(digitalRead(slagboomButtonExternalPin) && !stopSlagboomMotor) {
    openSlagboom();
    slagboomOpenedExternaly = true;
  }
  if(digitalRead(slagboomButtonInternalPin) && !stopSlagboomMotor) {
    openSlagboom();
    slagboomOpenedExternaly = false;
  }
  if(digitalRead(slagboomButtonInternalPin) && slagboomOpened) closeSlagboom(); 
} 
  
void openSlagboom() {
  if(!slagboomOpened && !slagboomStateChanging) {
    Serial.println("Open Slagboom");
    slagboomStateChanging = true;
    // open slagboom
    ESC.writeMicroseconds(2000); // waarde kan nog veranderen
  }
  if(digitalRead(slagboomTriggerButtonPin)) {
    Serial.println("Slagboom is open");
    slagboomOpened = true;
    slagboomStateChanging = false;
    slagboomOpenedTime = millis();
    stopSlagboomMotor = false;
    digitalWrite(signalingPin, LOW);
    // stop motor 
    ESC.writeMicroseconds(1000); // waarde kan nog veranderen
  }
} 

void closeSlagboom() {
  
  if(slagboomOpened && !slagboomStateChanging) {
    Serial.println("Sluit Slagboom");
    slagboomStateChanging = true;
    // sluit slagboom
    ESC.writeMicroseconds(1250); // waarde kan nog veranderen
  }
  if(digitalRead(slagboomTriggerButtonPin)) {
    Serial.println("Slagboom is gesloten");
    slagboomOpened = false;
    slagboomStateChanging = false;
    stopSlagboomMotor = false;
    digitalWrite(signalingPin, LOW);
    // stop motor 
    ESC.writeMicroseconds(1000); // waarde kan nog veranderen
  }
}

void autoCloseSlagboom() {
  if(slagboomOpenedExternaly && slagboomOpened) {
    if(slagboomOpenedTime + 60000 <= millis()) closeSlagboom();
    else digitalWrite(signalingPin, HIGH);
  }
}

void stopClosingSlagboom() {
  stopSlagboomMotor = true;
  slagboomStateChanging = false;
  slagboomOpened = false;
  digitalWrite(signalingPin, LOW);
  Serial.println("Slagboom gestopt");
  openSlagboom();
}
