long prevMillis;

int interval = 1000;

void knipperlicht() {
    if(millis() - prevMillis >= interval) {
      if(digitalRead(signalingPin)) digitalWrite(signalingPin, LOW);
      else digitalWrite(signalingPin, HIGH);
      prevMillis = millis();
    }
}
