int distance;

long duration;

void checkSensor() {
  if(slagboomBlocked()) stopClosingSlagboom();
}

bool slagboomBlocked() {
  bool isBlocked;
  // controleer of er iemand onder de sensor staat
  // https://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/ 
  digitalWrite(trigPin, LOW);
  // 2 micro delay
  digitalWrite(trigPin, HIGH);
  // 10 mirco delay
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration*0.034/2;
  if(distance > 300) distance = 0;
  if(distance < 30) isBlocked = true; // als er iets binnen de 30cm
  return isBlocked;
}
