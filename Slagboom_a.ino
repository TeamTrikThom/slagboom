long intervalTime;
long buttonTime;
// moet een long zijn omdat er grote getallen mogelijk zijn

int debounce = 200;
int interval = 1000;
int fietslampPin = 13;
int buttonPin = 2;
int potentioPin = 5;

bool trigger; // kan enkel true(1) of false(0) zijn

void setup() {
  // definieer de uitgangen
  pinMode(fietslampPin, OUTPUT);
  pinMode(buttonPin, INPUT);
  pinMode(potentioPin, INPUT);
}

void loop() {
  // waarde van de potentiometer inlezen en remappen
  interval = map(analogRead(potentioPin), 0, 1023, 250, 1000);
  // als de knop wordt ingedrukt en er is een groter verschil dan de debounce
  if(digitalRead(buttonPin) && millis() - buttonTime > debounce) {
    if(trigger) {
      // lamp uit omdat hij anders aan blijft
      trigger = false;
      digitalWrite(fietslampPin, LOW);
    }
    else trigger = true;
    // tijd opslaan om de volgende keer te controleren
    buttonTime = millis();
  }
  
  // als de trigger aan is en de tijd groter of gelijk is aan het interval
  if((millis() - intervalTime >= interval) && trigger) {
    // als de lamp aan is dan doe hem uit en andersom
    if(digitalRead(fietslampPin)) digitalWrite(fietslampPin, LOW);
    else digitalWrite(fietslampPin, HIGH);
    // tijd opslaan om de volgende keer te controleren
    intervalTime = millis();
  }
}
